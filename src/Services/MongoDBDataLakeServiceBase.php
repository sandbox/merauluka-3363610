<?php

namespace Drupal\mongodb_data_lake\Services;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\mongodb\DatabaseFactory;
use MongoDB\Driver\Cursor;
use MongoDB\Model\BSONDocument;
use Psr\Log\LoggerInterface;

/**
 * The MongoDB Data Lake service base.
 *
 * Common base variables and methods for MongoDB Data Lake access.
 */
class MongoDBDataLakeServiceBase extends ServiceProviderBase {

  /**
   * Number of seconds until the cache should expire - negative means permanent.
   *
   * @var int
   */
  protected $cacheExpiration = -1;

  /**
   * MongoDB database factory service.
   *
   * @var \Drupal\mongodb\DatabaseFactory
   */
  protected $mongoDBDatabaseFactory;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Logs to the mongodb_data_lake channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;


  /**
   * Collection.
   *
   * @var array
   */
  protected $collection;

  /**
   * Constructs an MongoDBDataLakeServiceBase instance.
   *
   * @param \Drupal\mongodb\DatabaseFactory $mongodb_database_factory
   *   The mongodb database factory service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logs to the mongodb_data_lake channel.
   */
  public function __construct(
    DatabaseFactory $mongodb_database_factory,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cache,
    LoggerInterface $logger) {
    $this->mongoDBDatabaseFactory = $mongodb_database_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->cache = $cache;
    $this->logger = $logger;

    // Get server backends of the type mongodb_data_lake_data_lake.
    /** @var \Drupal\search_api\ServerInterface[] $servers */
    $servers = $this->entityTypeManager
      ->getStorage('search_api_server')
      ->loadByProperties(['backend' => 'mongodb_data_lake_data_lake']);
    // If there is no search_api server with the data lake backend.
    if (!$servers) {
      // Log the error.
      $logger->error('Could not find a search_api server with backend type "mongodb_data_lake_data_lake".');
    }
    else {
      // Get the backend configuration for the first server found.
      // @todo Don't assume there is only one. This may require additional config.
      $server_backend_config = current($servers)->getBackendConfig();
      try {
        // Get the database based on the backend configuration.
        if (!$database = $this->mongoDBDatabaseFactory->get($server_backend_config['database_alias'])) {
          $logger->error('Cannot access the MongoDB Data Lake "%database".', ['%database' => $server_backend_config['database_alias']]);
          return;
        }
        // Get the database collection based on the backend configuration.
        if (!$this->collection = $database->selectCollection($server_backend_config['connector_config']['collection'])) {
          $logger->error('Cannot access the MongoDB Data Lake collection for database: %database and collection: %collection.', [
            '%database' => $server_backend_config['database_alias'],
            '%collection' => $server_backend_config['connector_config']['collection'],
          ]);
          return;
        }
        // If the collection is empty, log a message.
        if ($this->collection->countDocuments() == 0) {
          $logger->error('The MongoDB Data Lake collection, %collection, is empty.', ['%collection' => $server_backend_config['connector_config']['collection']]);
        }
      }
      catch (\Exception $e) {
        // Log the exception.
        $this->logger->error($e->getMessage());
        return;
      }
    }
  }

  /**
   * Execute the query and return the data.
   *
   * @param string $type
   *   The type of query.
   * @param array $filter
   *   The filter items for the query.
   * @param array $options
   *   The options items for the query.
   *
   * @return array|null
   *   Array of results from cache or database.
   */
  protected function query($type, array $filter, array $options) {
    // Initialize the data array.
    $data = [];
    // Set the query cache key.
    $key = $this->getQueryCacheKey($type, $filter, $options);
    // Attempt to load the cache key.
    $cache_key = $this->getCacheKeyFromArray($key);
    // If you can load data from cache, return it.
    if ($cache = $this->cache->get($cache_key)) {
      return $cache->data;
    }
    try {
      // Run the query and convert results based on the type.
      switch ($type) {
        case "find":
          // Run the query to find multiple results.
          $cursor = $this->collection->find($filter, $options);
          // If there is are results.
          if ($cursor instanceof Cursor) {
            // Convert the results to an array.
            $data = $this->convertCursorToArray($cursor);
          }
          break;

        case "findOne":
          // Run the query to find one result.
          $bson_document = $this->collection->findOne($filter, $options);
          // If data was found.
          if ($bson_document instanceof BSONDocument) {
            // Convert the results to an array.
            $data = $this->convertBsonDocumentToArray($bson_document);
          }
          break;

      }
      // If the collection isn't empty, cache the empty results. If it's empty
      // something is wrong and we don't want to cache the results.
      if ($this->collection->countDocuments() > 0) {
        $expiration = $this->cacheExpiration < 0
          ? $this->cacheExpiration : time() + $this->cacheExpiration;
        // Store the data in cache.
        $this->cache->set(
          $cache_key,
          $data,
          $expiration
        );
      }
      // Return the data.
      return $data;
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
      return NULL;
    }
  }

  /**
   * Get the cache key for Data Lake queries.
   *
   * @param string $type
   *   The type of query.
   * @param array $filter
   *   The filter items for the query.
   * @param array $options
   *   The options items for the query.
   *
   * @return array
   *   The query cache key array.
   */
  protected function getQueryCacheKey($type, array $filter, array $options) {
    // Return the query cache key.
    return [$type, $filter, $options];
  }

  /**
   * Get a cache key from an array.
   *
   * @param array $key
   *   The array used to create a cache key.
   *
   * @return string
   *   The cache key.
   */
  public function getCacheKeyFromArray(array $key) {
    $hash = hash('sha256', serialize($key));
    return "mongodb_data_lake_service:$hash";
  }

  /**
   * Convert the Cursor results to a multidimensional array.
   *
   * @param \MongoDB\Driver\Cursor $cursor
   *   The MongoDB Cursor object.
   *
   * @return array
   *   Array of results.
   */
  public function convertCursorToArray(Cursor $cursor) {
    // Convert a MongoDB\Driver\Cursor to array.
    $objects = Json::decode(Json::encode($cursor->toArray()));
    // Convert stdClass to array.
    return Json::decode(Json::encode($objects));
  }

  /**
   * Convert the BSONDocument results to a multidimensional array.
   *
   * @param \MongoDB\Model\BSONDocument $bson_document
   *   The MongoDB BSONDocument object.
   *
   * @return array
   *   Array of results.
   */
  public function convertBsonDocumentToArray(BSONDocument $bson_document) {
    // Convert a MongoDB\Driver\BSONDocument to array.
    return Json::decode(Json::encode($bson_document));
  }

  /**
   * Get an array of content types that are indexed in the Data Lake.
   *
   * @return array
   *   An array of content types: keys are machine name, values are label.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDataLakeIndexedContentTypes() {
    // Get server backends of the type mongodb_data_lake_data_lake.
    /** @var \Drupal\search_api\ServerInterface[] $servers */
    $servers = $this->entityTypeManager
      ->getStorage('search_api_server')
      ->loadByProperties(['backend' => 'mongodb_data_lake_data_lake']);
    // If no server is found, return an empty array.
    if (!$servers) {
      return [];
    }
    // Make an assumption that there is only one Data Lake server.
    $server_id = current($servers)->id();
    /** @var \Drupal\search_api\IndexInterface[] $indexes */
    $indexes = $this->entityTypeManager
      ->getStorage('search_api_index')
      ->loadByProperties(['server' => $server_id]);
    // If there is no search_api indexes for the data lake server return empty
    // array.
    if (!$indexes) {
      return [];
    }
    // Initialize the return array.
    $content_types = [];
    // Loop through indexes.
    foreach ($indexes as $index) {
      // Get the data source information from the index.
      $data_sources = $index->getDatasources();
      // Get the specific data source info.
      $source = current($data_sources);
      // If the source entity is a node.
      if ($source->getEntityTypeId() === 'node') {
        // Get the bundles array.
        $bundles = $source->getBundles();
        $content_types = $content_types + $bundles;
      }
    }
    return $content_types;
  }

}
