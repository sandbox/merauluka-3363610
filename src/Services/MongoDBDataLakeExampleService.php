<?php

namespace Drupal\mongodb_data_lake\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\mongodb\DatabaseFactory;
use MongoDB\BSON\Regex;
use Psr\Log\LoggerInterface;

/**
 * A MongoDB Data Lake service example.
 *
 * Access content in the MongoDB Data Lake database.
 */
class MongoDBDataLakeExampleService extends MongoDBDataLakeServiceBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    DatabaseFactory $mongodb_database_factory,
    EntityTypeManager $entity_type_manager,
    CacheBackendInterface $cache,
    LoggerInterface $logger) {
    // Set the cache expiration to 15 minutes.
    $this->cacheExpiration = 900;
    // Run the parent constructor.
    parent::__construct(
      $mongodb_database_factory,
      $entity_type_manager,
      $cache,
      $logger);
  }

  /**
   * Return filtered resource data by title text.
   *
   * @param string $title
   *   Text to search in 'title' field.
   * @param int $offset
   *   Optional. If given, the result will start with this result.
   * @param int $limit
   *   Optional. If given, the result will be limited to this count.
   *
   * @return array
   *   An array of results.
   */
  public function getDataByTitle($title = '', $offset = 0, $limit = 0) {
    // Set filter for the query.
    $filter = [];
    if ($title) {
      // Create a Regex object to find any title containing given value.
      // The "i" flag searches as case-insensitive.
      $title_regex = new Regex("$title", "i");
      // Filter the title field by the Regex.
      $filter = [
        'title' => $title_regex,
      ];
    }
    // Set options for the query.
    $options = [
      'projection' => [
        '_id' => 1,
        'siteId' => 1,
        'title' => 1,
      ],
    ];
    if ($limit) {
      $options['limit'] = $limit;
    }
    if ($offset) {
      $options['skip'] = $offset;
    }
    // Run the cacheable query and return results.
    return $this->query('find', $filter, $options);
  }

  /**
   * Return a single resource item data for a given Data Lake ID.
   *
   * @param string $id
   *   Data Lake ID.
   *
   * @return array
   *   A data array for the item.
   */
  public function getDataById($id) {
    // Filter by id.
    $filter = [
      '_id' => $id,
    ];
    // Intialize $options.
    $options = [];
    // Run the cacheable query and return results.
    return $this->query('findOne', $filter, $options);
  }

  /**
   * Returns the query cache key for a given id.
   *
   * @param string $id
   *   Data Lake ID.
   *
   * @return string
   *   The query cache key.
   */
  public function getDataByIdQueryCacheKey($id) {
    // Set the type value. See the findOne() method in the base class.
    $type = 'findOne';
    // Set the filter value. See the getDataById() method.
    $filter = [
      '_id' => $id,
    ];
    // Set the options value. See the getDataById() method.
    $options = [];
    // Return the query cache key.
    $key = $this->getQueryCacheKey($type, $filter, $options);
    // Generate and return the cache key.
    return $this->getCacheKeyFromArray($key);
  }

}
