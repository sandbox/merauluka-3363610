<?php

namespace Drupal\mongodb_data_lake\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Site\Settings;
use Drupal\mongodb\ClientFactory;
use Drupal\mongodb\DatabaseFactory;
use Drupal\mongodb_data_lake\Events\DataLakeEvents;
use Drupal\mongodb_data_lake\Events\DataLakePreprocessDocumentsEvent;
use Drupal\mongodb_data_lake\Utility;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Query\QueryInterface;
use MongoDB\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * MongoDB Data Lake backend for search api.
 *
 * @SearchApiBackend(
 *   id = "mongodb_data_lake_data_lake",
 *   label = @Translation("Data Lake"),
 *   description = @Translation("Index items using the MongoDB Data Lake backend.")
 * )
 */
class DataLakeBackend extends BackendPluginBase implements PluginFormInterface {

  /**
   * The Client factory service.
   *
   * @var \Drupal\mongodb\ClientFactory
   */
  protected $clientFactory;

  /**
   * The mongodb settings defining connection information.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $collection;

  /**
   * The mongodb settings defining connection information.
   *
   * @var \Drupal\mongodb\DatabaseFactory
   */
  protected $databaseFactory;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The mongodb settings defining connection information.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $mongoDbSettings;

  /**
   * Direct reference to the server's $options property.
   *
   * @var array
   */
  protected $options = [];

  /**
   * The custom Data Lake Backend constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Site\Settings $settings
   *   The Drupal settings service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\mongodb\ClientFactory $client_factory
   *   The mongodb client factory.
   * @param \Drupal\mongodb\DatabaseFactory $database_factory
   *   The mongodb database factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, Settings $settings, LoggerChannelFactoryInterface $logger_factory, ClientFactory $client_factory, DatabaseFactory $database_factory, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->logger = $logger_factory->get('mongodb_data_lake');
    $this->mongoDbSettings = $settings->get('mongodb');
    $this->clientFactory = $client_factory;
    $this->databaseFactory = $database_factory;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   *
   * Adds a logger channel identified with our module.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('settings'),
      $container->get('logger.factory'),
      $container->get('mongodb.client_factory'),
      $container->get('mongodb.database_factory'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Load the configured database connection for the Data Lake.
   *
   * @param string $database_alias
   *   The name of the database to target in the data lake.
   *
   * @return \MongoDB\Database|null
   *   The selected database, or NULL if an error occurred.
   */
  private function getDatabaseConnection($database_alias) {
    return $this->databaseFactory->get($database_alias);
  }

  /**
   * Load the configured database and collection to use for indexing.
   *
   * @param string $database_alias
   *   The configured database name.
   * @param string $collection_name
   *   The configured collection name.
   *
   * @return Collection
   *   The MongoDB collection to use for indexing operations.
   */
  private function getCollection($database_alias, $collection_name) {
    $database = $this->getDatabaseConnection($database_alias);
    return $database->selectCollection($collection_name);
  }

  /**
   * Process any configured Mongo DB connection options.
   *
   * @return array
   *   The array of options for the configuration form.
   */
  private function getAvailableMongoDbInstances() {
    $options = [];
    if (empty($this->mongoDbSettings['databases'])) {
      return $options;
    }
    foreach ($this->mongoDbSettings['databases'] as $key => $values) {
      $value = str_replace('_', ' ', $key);
      $options[$key] = ucwords($value);
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Allow mongodb destination to be selected from the list of configured
    // mongodb sources. If none are available, then display a message and
    // prevent the creation of the index.
    $mongodb_options = $this->getAvailableMongoDbInstances();
    $form['database_alias'] = [
      '#type' => 'radios',
      '#title' => $this->t('MongoDB database'),
      '#description' => $this->t('Choose a destination for the data lake values.'),
      '#options' => $mongodb_options,
      '#default_value' => $this->configuration['database_alias'] ?? '',
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [get_class($this), 'buildAjaxDataLakeConfigForm'],
        'wrapper' => 'mongodb-data-lake-config-form',
        'method' => 'replace',
        'effect' => 'fade',
      ],
    ];

    $this->buildConnectorConfigForm($form, $form_state);

    return $form;
  }

  /**
   * Builds the backend-specific configuration form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildConnectorConfigForm(array &$form, FormStateInterface $form_state) {
    $form['connector_config'] = [];

    $database_alias = $this->configuration['database_alias'] ?? '';
    if ($database_alias) {
      $database = $this->getDatabaseConnection($database_alias);
      if ($database instanceof Database) {
        $form_state->set('database_alias', $database_alias);
        if ($form_state->isRebuilding()) {
          \Drupal::messenger()->addWarning($this->t('Please configure the selected Data Lake database.'));
        }
        $collections = $database->listCollectionNames();

        // Modify the backend plugin configuration container element.
        $form['connector_config']['#type'] = 'details';
        $form['connector_config']['#title'] = $this->t('Select a collection on the %database_alias database:', ['%database_alias' => $database_alias]);
        $form['connector_config']['#open'] = TRUE;
        if (empty($collections)) {
          $form['connector_config']['collection'] = [
            '#type' => 'markup',
            '#markup' => $this->t('<em>No collections found on this database.</em>'),
          ];
        }
        else {
          $collection_options = [];
          foreach ($collections as $collection) {
            $collection_options[$collection] = $collection;
          }
          asort($collection_options);
          $form['connector_config']['collection'] = [
            '#type' => 'radios',
            '#options' => $collection_options,
            '#default_value' => $this->configuration['connector_config']['collection'] ?? '',
            '#required' => TRUE,
          ];
        }
      }
    }
    $form['connector_config'] += ['#type' => 'container'];
    $form['connector_config']['#attributes'] = [
      'id' => 'mongodb-data-lake-config-form',
    ];
    $form['connector_config']['#tree'] = TRUE;

  }

  /**
   * Handles switching the selected connector plugin.
   */
  public static function buildAjaxDataLakeConfigForm(array $form, FormStateInterface $form_state) {
    // The work is already done in form(), where we rebuild the entity according
    // to the current form values and then create the backend configuration form
    // based on that. So we just need to return the relevant part of the form
    // here.
    return $form['backend_config']['connector_config'];
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function viewSettings() {
    $database_alias = $this->configuration['database_alias'];
    [$client_alias, $database_name] = $this->mongoDbSettings['databases'][$database_alias];

    // Load the database connection to check the status.
    $connection = $this->getDatabaseConnection($database_alias);
    if (empty($connection)) {
      $info[] = [
        'label' => $this->t('Database status'),
        'info' => $this->t('<em>Unable to connect to the configured database.</em>'),
      ];
    }
    else {
      $client = $this->clientFactory->get($client_alias);
      $uri = $client->__toString();
      if ($auth_length = strpos($uri, '@')) {
        $auth_string = substr($uri, 0, $auth_length);
        $auth_string = substr($auth_string, strrpos($auth_string, '/') + 1);
        $uri = str_replace($auth_string, '****:****', $uri);
      }
      $info[] = [
        'label' => $this->t('Database uri'),
        'info' => $uri,
      ];
    }
    $info[] = [
      'label' => $this->t('Database name'),
      'info' => $database_name,
    ];

    return $info;
  }

  /**
   * Returns the combined site hash + document id for indexing.
   *
   * @param string $id
   *   The document id (usually the UUID from Drupal).
   *
   * @return string
   *   The site hash + document id for mongodb.
   */
  private function getDocumentId(string $id) {
    $site_hash = Utility::getSiteHash();
    return $site_hash . ':' . $id;
  }

  /**
   * Load the array of document data to pass through to mongo db.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search api index.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   The loaded items for indexing.
   *
   * @return array
   *   The array of item data to index.
   */
  public function getFormattedDocuments(IndexInterface $index, array $items) {
    $documents = [];
    foreach ($items as $item) {
      $document = [];
      $fields = $item->getFields();
      foreach ($fields as $field) {
        $field_name = $field->getFieldIdentifier();
        $type = $field->getType();
        $field_values = $field->getValues();
        if ($type == 'text') {
          if (!empty($field_values)) {
            $field_values = reset($field_values);
            $value = $field_values->getText();
          }
          else {
            $value = '';
          }
        }
        elseif (is_array($field_values) && count($field_values) <= 1) {
          $value = reset($field_values);
        }
        else {
          $value = $field_values;
        }

        $document[$field_name] = $value;

        if ($field_name == 'id') {
          $document['_id'] = $this->getDocumentId($value);
        }

      }
      // Set custom values for Search API and Drupal to use.
      $document['search_api_id'] = $item->getId();
      $document['source_id'] = Utility::getSiteHash();
      $documents[$item->getId()] = $document;
    }
    return $documents;
  }

  /**
   * Indexes the specified items.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The search index for which items should be indexed.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   An array of items to be indexed, keyed by their item IDs.
   *
   * @return string[]
   *   The IDs of all items that were successfully indexed.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if indexing was prevented by a fundamental configuration error.
   */
  public function indexItems(IndexInterface $index, array $items) {
    $configuration = $this->configuration;
    $collection = $this->getCollection($configuration['database_alias'], $configuration['connector_config']['collection']);
    $loaded_documents = $this->getFormattedDocuments($index, $items);

    // Allow custom modules to provide custom data and transforms for the Data
    // Lake prior to indexing.
    $event = new DataLakePreprocessDocumentsEvent($loaded_documents, $items, $index);
    $this->eventDispatcher->dispatch(DataLakeEvents::PREPROCESS_DOCUMENTS, $event, $index);
    $processed_documents = $event->getDocuments();

    $indexed_ids = [];
    if ($processed_documents) {
      foreach ($processed_documents as $document) {
        try {
          // Attempt to replace the documents in the database. This allows for
          // the structure of MongoDB which doesn't like attempting to insert
          // a record that already exists (based on the _id value). The replace
          // method attempts to find a matching record to replace with the
          // incoming record and falls back on inserting a new record if no
          // match is found.
          $collection->replaceOne(['_id' => $this->getDocumentId($document['id'])], $document, ['upsert' => TRUE]);
          $indexed_ids[] = $document['search_api_id'];
        }
        catch (\Exception $e) {
          watchdog_exception('mongodb_data_lake', $e);
        }
      }
    }
    return $indexed_ids;
  }

  /**
   * Deletes the specified items from the index.
   *
   * @param \Drupal\search_api\IndexInterface $index
   *   The index from which items should be deleted.
   * @param string[] $item_ids
   *   The IDs of the deleted items.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error occurred while trying to delete the items.
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    $configuration = $this->configuration;
    $collection = $this->getCollection($configuration['database_alias'], $configuration['connector_config']['collection']);
    foreach ($item_ids as $item_id) {
      try {
        // Since a single search_api_id might be duplicated if the collection
        // is shared between many sites, limit the deletion to this site only.
        $collection->deleteOne([
          'search_api_id' => $item_id,
          'source_id' => Utility::getSiteHash(),
        ]);
      }
      catch (\Exception $e) {
        watchdog_exception('mongodb_data_lake', $e);
      }
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\search_api\SearchApiException
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    $configuration = $this->configuration;
    $collection = $this->getCollection($configuration['database_alias'], $configuration['connector_config']['collection']);
    try {
      // MongoDB only requires an empty query in the DeleteMany command to wipe
      // the entire collection. Limit the deletion to only records that are
      // indexed for this particular site.
      $source_id = Utility::getSiteHash();
      // Only remove records for the given index. The search api index id must
      // be mapped to the schema as 'search_api_index_id'.
      $search_api_index_id = $index->id();
      $collection->deleteMany([
        'source_id' => $source_id,
        'search_api_index_id' => $search_api_index_id,
      ]);
    }
    catch (\Exception $e) {
      watchdog_exception('mongodb_data_lake', $e);
    }
  }

  /**
   * Executes a search on this server.
   *
   * @param \Drupal\search_api\Query\QueryInterface $query
   *   The query to execute.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   Thrown if an error prevented the search from completing.
   */
  public function search(QueryInterface $query) {
    // Search will be performed by a front end application, not through Drupal.
  }

  /**
   * Determines whether the service class implementation supports given feature.
   *
   * Features are optional extensions to Search API functionality and
   * usually defined and used by third-party modules.
   *
   * Currently, the only feature specified directly in the search_api project is
   * "search_api_facets", defined by the module of the same name.
   *
   * @param string $feature
   *   The name of the optional feature.
   *
   * @return bool
   *   TRUE if this service knows and supports the specified feature. FALSE
   *   otherwise.
   */
  public function supportsFeature($feature) {
    return FALSE;
  }

}
