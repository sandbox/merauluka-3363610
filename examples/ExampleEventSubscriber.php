<?php

namespace Drupal\example_custom_module\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;
use Drupal\mongodb_data_lake\Events\DataLakeEvents;
use Drupal\mongodb_data_lake\Events\DataLakePreprocessDocumentsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Apply any necessary transforms to data before it is indexed to the Data Lake.
 */
class ExampleEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The settings object.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * The value of the product's canonical URL (product info page).
   *
   * @var string|null
   */
  protected $productUrl = NULL;

  /**
   * The value of productUrl, but only the slug (everything after /products/).
   *
   * @var string|null
   */
  protected $productUrlFragment = NULL;

  /**
   * Constructs a ContentEntityTaskManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Settings $settings) {
    $this->entityTypeManager = $entity_type_manager;
    $this->settings = $settings;
  }

  /**
   * Returns the schema defined for this data lake set.
   *
   * @return array
   *   The array of values to use when mapping the incoming data for indexing.
   */
  protected function getSchemaTemplate() {
    return [
      // Data Lake required fields.
      'uuid' => NULL,
      'title' => NULL,
      'created' => NULL,
      'updated' => NULL,
      'sourceSystem' => 'my_system',
      'expiration' => 0,
      'cacheability' => '300',
      // Project-specific fields.
      'name' => NULL,
      'id' => NULL,
      'categories' => [],
      'keywords' => [],
      'description' => NULL,
      'longDescription' => NULL,
      'dataSource' => NULL,
      'url' => NULL,
      'schemaVersion' => '1.0.0-alpha2',
      'sourceUniqueId' => NULL,
      'revisionId' => NULL,
      'language' => NULL,
      'locales' => [],
      // Search API values.
      'source_id' => NULL,
      'search_api_id' => NULL,
    ];
  }

  /**
   * The basic mapping to ensure translated fields are processed into locales.
   *
   * @return array
   *   The array of mapped field values.
   */
  protected function getTranslationMap() {
    return [
      'value' => [
        'title' => 'name',
        'changed' => 'updated',
        'field_description' => 'description',
        'field_long_description' => 'longDescription',
      ],
      'term' => [
        'field_category' => 'categories',
      ],
    ];
  }

  /**
   * Applies the Data Lake schema to values from the Search API index.
   *
   * @param \Drupal\mongodb_data_lake\Events\DataLakePreprocessDocumentsEvent $event
   *   The triggered custom preprocess event.
   */
  public function applySchema(DataLakePreprocessDocumentsEvent $event) {
    $schema = $this->getSchemaTemplate();
    $documents = $event->getDocuments();
    foreach ($documents as $key => $document) {
      $documents[$key] = $this->processSchema($document, $schema);
    }
    $event->setDocuments($documents);
  }

  /**
   * Map incoming values from Search API to the Data Lake schema.
   *
   * @param mixed $document
   *   Either an array or a value to be sent on the document.
   * @param array $schema
   *   The structured schema template.
   *
   * @return array
   *   The array of processed values.
   */
  protected function processSchema($document, array $schema) {
    $processed_document = $schema;
    $node_storage = $this->entityTypeManager->getStorage('node');
    $product = $node_storage->loadRevision($document['revisionid']);

    foreach ($schema as $field_name => $value) {
      $field_name_lower = strtolower($field_name);
      if (is_array($value) && !empty($value)) {
        $processed_document[$field_name] = $this->processSchema($document, $value);
      }
      elseif ($field_name == 'locales') {
        $processed_document[$field_name] = $this->buildLocalesObject($product, $schema);
      }
      elseif ($field_name == 'categories' && !is_array($document['categories']) && !empty($document['categories'])) {
        $processed_document[$field_name] = [$document[$field_name]];
      }
      elseif ($field_name == 'keywords') {
        $processed_document[$field_name] = $this->convertToArray($document[$field_name], ',');
      }
      elseif (isset($document[$field_name_lower])) {
        $processed_document[$field_name] = $document[$field_name_lower];
      }
    }
    return $processed_document;
  }

  /**
   * Build the locales object for all translated values on the node.
   *
   * @param \Drupal\node\NodeInterface $product
   *   The node to use when building the locale data.
   * @param array $schema
   *   The default structured schema template.
   *
   * @return array
   *   The array of built locales data.
   */
  private function buildLocalesObject(NodeInterface $product, array $schema) {
    // Load the locale-specific versions of this document (node).
    $translated_fields = $this->getTranslationMap();
    $localized_products = [
      'en' => $product,
      'ko' => $product->hasTranslation('ko') ? $product->getTranslation('ko') : NULL,
      'ja' => $product->hasTranslation('ja') ? $product->getTranslation('ja') : NULL,
      'zh-hans' => $product->hasTranslation('zh-hans') ? $product->getTranslation('zh-hans') : NULL,
    ];
    $locales = [];
    foreach ($localized_products as $langcode => $localized_product) {
      if (empty($localized_product) || empty($localized_product->status->value)) {
        continue;
      }
      $locale = ['locale' => $langcode];
      foreach ($translated_fields as $type => $mapping) {
        switch ($type) {
          case 'value':
            foreach ($mapping as $drupal_field => $data_lake_field) {
              $locale[$data_lake_field] = $localized_product->{$drupal_field}->value;
            }
            break;

          case 'term':
            foreach ($mapping as $drupal_field => $data_lake_field) {
              $term = $localized_product->{$drupal_field}->entity;
              if (!empty($term) && $term->hasTranslation($langcode)) {
                $term = $term->getTranslation($langcode);
              }
              if (is_array($schema[$data_lake_field])) {
                $locale[$data_lake_field] = !empty($term) ? [$term->label()] : [];
              }
              else {
                $locale[$data_lake_field] = !empty($term) ? $term->label() : NULL;
              }
            }
            break;

        }
      }
      $locales[] = $locale;
    }
    return $locales;
  }

  /**
   * Converts a string/text area into an array.
   */
  private function convertToArray($value, $separator) {
    if (empty($value)) {
      return [];
    }
    $exploded_values = explode($separator, $value);
    return array_map('trim', $exploded_values);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Set priority to a low number since this is a custom event that won't
    // initially have any competing events.
    $events[DataLakeEvents::PREPROCESS_DOCUMENTS][] = ['applySchema', 10];
    return $events;
  }

}
