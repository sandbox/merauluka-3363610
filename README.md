# Contents of this file

* Setup Guidance
* Requirements
* Maintainers

## Setup Guidance

Install and enable the MongoDB Data Lake module on a D9+ site. _It will
automatically install all necessary required modules._

### MongoDB Instance

In order to index into a data lake, you will need to have an available MongoDB
instance in place with a collection provisioned to accept the incoming data.

### MongoDB Settings

Add a settings entry for the `$settings['mongodb']` value in the appropriate
Drupal `settings.php` file. The structure of this file is defined by the Drupal
MongoDB module.

_These settings are based on requirements from the Drupal MongoDB module._

#### Example
```php
    $settings['mongodb'] = [
      'clients' => [
        'database_clients_key' => [
          'uri' => 'mongodb://url-to-mongo-instance',
          'uriOptions' => [
            'username' => 'mongo-username',
            'password' => 'mongo-password',
            'slaveOk' => TRUE,
            'ssl' => FALSE,
            'authSource' => 'admin',
          ],
          'driverOptions' => [],
        ],
      ],
      'databases' => [
        'drupal_internal_db_key' => ['database_clients_key', 'database_name'],
      ],
    ];
```

### Unique Site Hash
The default site_hash value uses current site's `global $base_url` value. If you
need your indexes to be unique per site but not per environment, you can
override the hash value in settings.php.

We recommend using a per-project code to ensure uniqueness of data lake records.

```
$settings['mongodb_data_lake.site_hash'] = '<unique_project_code>';
```

To verify the setting is picked up by your site, check using Drush:
```bash
drush ev 'var_dump(\Drupal\Core\Site\Settings::get("mongodb_data_lake.site_hash"))'
```

### Local Development

#### Docksal updates

:stop_sign: Before you begin, make sure you've added the `mongodb_data_lake.site_hash` setting
to your `settings.php` file as describe above in the `Unique Site Hash` section.
This will be used locally and upstream.

##### Step 1: Update `settings.local.php`

Add the following to your `settings.local.php` file:
```php
    $settings['mongodb'] = [
        'clients' => [
            'default' => [
                'uri' => 'mongodb://mongodb',
                'uriOptions' => [
                    'username' => '<username>',
                    'password' => '<password>',
                    'slaveOk' => TRUE,
                    'ssl' => FALSE,
                    'authSource' => 'admin',
                ],
                'driverOptions' => [],
            ],
        ],
        'databases' => [
            'local' => ['default', '<database name>'],
        ],
    ];
```

Replace `<username>`, `<password>`, and `<database name>` with
the values of your choosing. They will be used later.

##### Step 2: Add Mongo containers

Add two new containers to your Docksal configuration for
`mongo` and `mongosh`.

In `.docksal/docksal.yml`, add the following:
```yml
    mongodb:
      image: mongo
      restart: always
      command: mongod --auth --quiet --bind_ip 0.0.0.0
      ports:
        - "${MONGODB_SERVICE_PORT:-27017}:27017"
      labels:
        - io.docksal.virtual-host=mongo.${VIRTUAL_HOST},mongo.${VIRTUAL_HOST}.*
        - io.docksal.virtual-port=27017
        - io.docksal.cert-name=${VIRTUAL_HOST_CERT_NAME:-none}
      environment:
        MONGO_INITDB_ROOT_USERNAME: '<username>'
        MONGO_INITDB_ROOT_PASSWORD: '<password>'
      volumes:
        - ../docker/mongodb:/data/db

    mongosh:
      image: rtsp/mongosh
      volumes:
        - ../mongosh:${PROJECT_ROOT}/mongosh
```

Replace `<username>` and `<password>` with the values used in `settings.local.php`.

If you have multiple Docksal projects adding a mongodb container and you'd like to access
the databases using a GUI tool, set `MONGODB_SERVICE_PORT` to a different value for each project
in the `docksal.env` or `docksal.local.env` files. A good second option is `27027`.

##### Step 2: Create MongoDB initialization script

Add a `mongosh` directory to your project root with a file called `mongodb_init.js`
containing initialization code like this:
```js
    use("admin");
    db.auth("<username>", "<password>");
    db.grantRolesToUser("<username>", [{role: "dbOwner", db:"admin"}, "userAdminAnyDatabase"]);
    use("<database name>");
    db.createCollection("<collection name>");
```

Replace `<username>`, `<password>`, and `<database name>` with the values used in `settings.local.php`.

Set `<collection name>` to the name of the existing or new collection you will be connecting to.
If this will be a new collection, use camelCase when selecting a name.

<a name="php-mongodb-ext"></a>
##### Step 3: Update Docksal `init` command

We need to update the `cli` container with PHP extensions and packages,
as well as run the initialization script we created earlier.

Add the following function to your initialization command (Example: `.docksal/commands/init`):
```bash
    # Initialize local mongodb.
    mongodb_init ()
    {
        # Install packages to enable libmongoc SSL.
        fin exec --in=cli 'sudo apt-get update && sudo apt-get install cmake libssl-dev libsasl2-dev -y'
        # Install the MongoDB PHP extension.
        fin exec --in=cli 'sudo pecl list | grep mongodb >>/dev/null || sudo pecl install mongodb && sudo -E docker-php-ext-enable mongodb'
        # Restart the cli container to pickup the changes.
        fin project restart cli
        # Add the necessary user name, password, roles, dbs, and collections for local mongodb development.
        fin exec --in=mongosh mongosh mongodb://mongodb --authenticationDatabase "admin" -u mongoadmin -p secret --file ${PROJECT_ROOT}/mongosh/mongodb_init.js
    }
```

If you have a command to rebuild containers, add this to clear out the local mongodb instance:

    # Remove the local MongoDB files.
    rm -rf ./docker/mongodb

If you have a custom command for when a project starts containers again, add this:

    # Initialization of MongoDB.
    mongodb_init

##### Step 4: Update root `.gitignore`

To ensure Git doesn't track MongoDB files, add the following to your root `.gitignore` file:

    # Ignore local docker/mongodb files.
    docker

## Requirements

### Prior to `composer require`:
* Composer patches
* [PHP MongoDB extension (CLI container)](#php-mongodb-ext)

### Module Dependencies
* MongoDB (https://www.drupal.org/project/mongodb)
* Search API (https://www.drupal.org/project/search_api)

## Maintainers

Current maintainers:
* Melissa Bent (merauluka) - https://drupal.org/u/merauluka
* April Sides (weekbeforenext) - https://drupal.org/u/weekbeforenext
