<?php

namespace Drupal\mongodb_data_lake\Events;

use Drupal\search_api\IndexInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps a document preprocess event for Data Lake indexing actions.
 */
final class DataLakePreprocessDocumentsEvent extends Event {

  /**
   * The search api documents being prepared to index.
   *
   * @var array
   */
  protected $documents;

  /**
   * The search api items being indexed.
   *
   * @var \Drupal\search_api\Item\ItemInterface[]
   */
  protected $items;

  /**
   * The search api index.
   *
   * @var \Drupal\search_api\IndexInterface
   */
  protected $index;

  /**
   * Constructs a new class instance.
   *
   * @param array $documents
   *   The array of document data about to be indexed.
   * @param \Drupal\search_api\Item\ItemInterface[] $items
   *   The search api items being indexed.
   * @param \Drupal\search_api\IndexInterface $index
   *   The search api index.
   */
  public function __construct(array &$documents, array $items, IndexInterface $index) {
    $this->documents = &$documents;
    $this->items = $items;
    $this->index = $index;
  }

  /**
   * Retrieves the array of search api items.
   *
   * @return array
   *   The array of search api documents to index.
   */
  public function &getDocuments() {
    return $this->documents;
  }

  /**
   * Retrieves the array of search api items.
   *
   * @param array $documents
   *   The array of search api documents to store.
   */
  public function setDocuments(array $documents) {
    $this->documents = $documents;
  }

  /**
   * Retrieves the array of search api items.
   *
   * @return array
   *   The array of search api documents to index.
   */
  public function getItems() {
    return $this->items;
  }

  /**
   * Retrieves the search api index.
   *
   * @return \Drupal\search_api\IndexInterface
   *   The search api index.
   */
  public function getIndex() {
    return $this->index;
  }

}
