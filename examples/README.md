# Example custom event subscriber

Once the MongoDB Data Lake module is installed, a custom event subscriber will
need to be added to process the new Data Lake event that is fired during
indexing operations performed by the Data Lake Backend.

You will need:
* Entry in your services.yml file in your custom module (see example_custom_module.services.yml)
* A new event subscriber located under you custom module in `src/EventSubscriber/` (see ExampleEventSubscriber.php)
