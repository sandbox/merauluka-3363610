<?php

namespace Drupal\mongodb_data_lake\Events;

/**
 * Defines events for the MongoDB Data Lake module.
 */
class DataLakeEvents {

  /**
   * The name of the event fired before indexing documents to the data lake.
   *
   * This allows other modules to alter schema and data mappings if necessary
   * when indexing into the MongoDB data lake.
   *
   * @Event
   *
   * @see \Drupal\mongodb_data_lake\Event\DataLakePreprocessDocumentsEvent
   */
  const PREPROCESS_DOCUMENTS = 'mongodb_data_lake.preprocess_documents';

}
